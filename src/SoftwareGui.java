import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SoftwareGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JFrame frame;
	JLabel Showname1, Showname2,result;
	JTextField text2;
	JTextArea text1, ShowResult;
	JPanel panel1, panel2, buttonPanel;
	JButton Enter;
	String str;
	JComboBox<String> select;
	JComboBox<?> comboBox;
	String[] names;

	String word;

	public SoftwareGui() {
		createGui();
	}

	public void createGui() {

		Showname1 = new JLabel("Enter your phase:");
		Showname2 = new JLabel("Enter n-gram:");
		result = new JLabel("Result");
		text1 = new JTextArea(2, 20);
		text2 = new JTextField(10);
		ShowResult = new JTextArea(5, 25);
		ShowResult.setEditable(false);
		panel1 = new JPanel();
		panel2 = new JPanel();
		buttonPanel = new JPanel();
		Enter = new JButton("Enter");
		
		
		String[] menu = new String[] {"", "Word Count", "Generate Ngram" };
		comboBox = new JComboBox<Object>(menu);
		comboBox.setEditable(true);
		
		panel1.add(Showname1);
		panel1.add(text1);

		panel1.add(Showname2);
		panel1.add(text2);
       
		panel2.add(result);
		panel2.add(ShowResult);

		// ShowResult.setEditable(false);
		setLayout(new BorderLayout());
		add(panel1, BorderLayout.NORTH);
		add(panel2, BorderLayout.CENTER);

		// add button in panel
		buttonPanel.add(comboBox);

		buttonPanel.add(Enter);
		add(buttonPanel, BorderLayout.SOUTH);
		
		ShowResult.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder ()));
		result.setVerticalTextPosition(JLabel.TOP);
		panel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder ()));
		text1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder ()));
		text2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder ()));
		
		
	}

	public void setResult(String str) {
		// TODO Auto-generated method stub
		this.str = str;
		ShowResult.setText(str);

	}

	public void setResult(int n) {
		// TODO Auto-generated method stub

	}

	public void extendResult(ArrayList<String> str) {
		// TODO Auto-generated method stub
		this.str = " " + str;
		ShowResult.setText(this.str);
	}

	public void setListener(ActionListener list) {
		// TODO Auto-generated method stub
		Enter.addActionListener(list);
		
		

	}
	
}
