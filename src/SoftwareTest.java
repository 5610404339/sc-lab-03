import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class SoftwareTest {

	class Listenermgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent evt) {

			gui.word = (String) gui.comboBox.getSelectedItem();

			String str1 = gui.text1.getText();
			String str2 = gui.text2.getText();

			if (gui.word == "Word Count") {
				gui.ShowResult.setText("" + ws.wordcount(str1));

			}
			if (gui.word == "Generate Ngram") {

				gui.ShowResult.setText(ws.generateNGram(str1, str2));

			}
			

		}

	}

	public static void main(String[] args) {
		new SoftwareTest();
	}

	public SoftwareTest() {
		gui = new SoftwareGui();
		ws = new WordSplit();
		gui.setSize(600, 500);
		gui.setTitle("Program Split Words ");
		gui.setLocation(400,270);
		gui.setVisible(true);
		gui.pack();
		list = new Listenermgr();
		gui.setListener(list);
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	SoftwareGui gui;
	ActionListener list;
	WordSplit ws;
}
